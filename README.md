# Automatic Extraction of Access Control Policies from Natural Language Documents


In order to run most part of this project, it is necessary to install python3, nltk 3,  simplejson and spacy python libraries. I recommend to use a python virtual environment, and install the packages in the virtual environment with pip3.

The other major libraries are:
 
 1) sci-kit-learn
 2) pandas
 3) numpy
 4) tensorflow
 5) tkinter

 An external dependency have to be exported via terminal for smooth functioning of tensorflow:

            export TF_CPP_MIN_LOG_LEVEL=2


The datasets to process are available  in 'datsets' folder in .csv format.

To run the program and visualize the output:

        1) python3 ACPEXTRACT.py

        2) Click on:

                Using the 'dep' visualizer
                Serving on http://0.0.0.0:5000 ...

            To visualize dependency parsed sentences.

        3) A json file named 'test1.json' will be generated which is the extracted ACP sentences,
        which is also available as a text file 'acp.txt'.


        
