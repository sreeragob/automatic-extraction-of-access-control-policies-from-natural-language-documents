Sentence,AccessControl
Course Registration Requirements,0
Version  2004,0
Problem Statement,0
As the head of information systems for Wylie College you are tasked with developing a new student registration system.,0
The college would like a new client-server system to replace its much older system developed around mainframe technology.,0
The new system will allow students to register for courses and view report cards from personal computers attached to the campus LAN.,1
Professors will be able to access the system to sign up to teach courses as well as record grades.,1
"Due to a decrease in federal funding, the college cannot afford to replace the entire system at once.",0
The college will keep the existing course catalog database where all course information is maintained.,0
This database is an Ingres relational database running on a DEC VAX.,0
Fortunately the college has invested in an open SQL interface that allows access to this database from college’s Unix servers.,0
"The legacy system performance is rather poor, so the new system must ensure that access to the data on the legacy system occurs in a timely manner.",0
The new system will access course information from the legacy database but will not update it.,1
The registrar’s office will continue to maintain course information through another system.,0
"At the beginning of each semester, students may request a course catalogue containing a list of course offerings for the semester.",1
"Information about each course, such as professor, department, and prerequisites, will be included to help students make informed decisions.",1
The new system will allow students to select four course offerings for the coming semester.,1
"In addition, each student will indicate two alternative choices in case the student cannot be assigned to a primary selection.",1
Course offerings will have a maximum of ten students and a minimum of three students.,0
A course offering with fewer than three students will be canceled.,0
"For each semester, there is a period of time that students can change their schedule.",1
Students must be able to access the system during this time to add or drop courses.,1
"Once the registration process is completed for a student, the registration system sends information to the billing system so the student can be billed for the semester.",1
"If a course fills up during the actual registration process, the student must be notified of the change before submitting the schedule for processing.",1
"At the end of the semester, the student will be able to access the system to view an electronic report card.",1
"Since student grades are sensitive information, the system must employ extra security measures to prevent unauthorized access.",0
Professors must be able to access the on-line system to indicate which courses they will be teaching.,1
They will also need to see which students signed up for their course offerings.,1
"In addition, the professors will be able to record the grades for the students in each class.",1
Introduction,0
Glossary,0
"This document is used to define terminology specific to the problem domain, explaining terms, which may be unfamiliar to the reader of the use-case descriptions or other project documents.",0
"Often, this document can be used as an informal data dictionary, capturing data definitions so that use-case descriptions and other project documents can focus on what the system must do with the information.",0
Definitions,0
The glossary contains the working definitions for the key concepts in the Course Registration System.,0
Course,0
A class offered by the university.,0
Course Offering,0
A specific delivery of the course for a specific semester – you could run the same course in parallel sessions in the semester.,0
Includes the days of the week and times it is offered.,0
Course Catalog,0
The unabridged catalog of all courses offered by the university.,0
Faculty,0
All the professors teaching at the university.,0
Finance System,0
The system used for processing billing information.,0
Grade,0
The evaluation of a particular student for a particular course offering.,0
Professor,0
A person teaching classes at the university.,0
Report Card,0
All the grades for all courses taken by a student in a given semester.,0
Roster,0
All the students enrolled in a particular course offering.,0
Student,0
A person enrolled in classes at the university.,0
Schedule,0
The courses a student has selected for the current semester.,0
Transcript,0
"The history of the grades for all courses, for a particular student sent to the finance system, which in turn bills the students.",0
Objectives,0
Supplementary Specification,0
The purpose of this document is to define requirements of the Course Registration System.,0
This Supplementary Specification lists the requirements that are not readily captured in the use cases of the use- case model.,0
The Supplementary Specifications and the use-case model together capture a complete set of requirements on the system.,0
Scope,0
"This Supplementary Specification applies to the Course Registration System, which will be developed by the OOAD students.",0
"This specification defines the non-functional requirements of the system; such as reliability, usability, performance, and supportability, as well as functional requirements that are common across a number of use cases.",0
(The functional requirements are defined in the Use Case Specifications.),0
References,0
None.,0
Functionality,0
Multiple users must be able to perform their work concurrently.,0
"If a course offering becomes full while a student is building a schedule including that offering, the student must be notified.",1
Usability,0
The desktop user-interface shall be Windows 95 or 98 compliant.,0
Reliability,0
"The system shall be available 24 hours a day 7 days a week, with no more than 10% down time.",0
Performance,0
"The system shall support up to 2000 simultaneous users against the central database at any given time, and up to 500 simultaneous users against the local servers at any one time.",0
The system shall provide access to the legacy course catalog database with no more than a 10 second latency.,0
Note: Risk-based prototypes have found that the legacy course catalog database cannot meet our performance needs without some creative use of mid-tier processing power,0
The system must be able to complete 80% of all transactions within 2 minutes.,0
Supportability,0
None.,0
Security,0
"The system must prevent students from changing any schedules other than their own, and professors from modifying assigned course offerings for other professors.",1
Only Professors can enter grades for students.,1
Only the Registrar is allowed to change any student information.,1
Design Constraints,0
"The system shall integrate with an existing legacy system, the Course Catalog System, which is an RDBMS database.",0
The system shall provide a Windows-based desktop interface.,0
Use-Case Model,0
Course Registration System Use-Case Model Main Diagram,0
Close Registration,0
Brief Description,0
This use case allows a Registrar to close the registration process.,1
Course offerings that do not have enough students are cancelled.,0
Course offerings must have a minimum of three students in them.,0
"The billing system is notified for each student in each course offering that is not cancelled, so the student can be billed for the course offering.",1
Flow of Events,0
Basic Flow,0
This use case starts when the Registrar requests that the system close registration.,1
1) ,The system checks to see if registration is in progress.
"If it is, then a message is displayed to the Registrar, and the use case terminates.",1
The Close Registration processing cannot be performed if registration is in progress.,1
"2) For each course offering, the system checks if a professor has signed up to teach the course offering and at least three students have registered.",1
"If so, the system commits the course offering for each schedule that contains it.",1
"3) For each schedule, the system levels the schedule: if the schedule does not have the maximum number of primary courses selected, the system attempts to select alternates from the schedule’s list of alternates.",1
The first available alternate course offerings will be selected.,1
"If no alternates are available, then no substitution will be made.",1
"4) For each course offering, the system closes all course offerings.",1
"If the course offerings do not have at least three students at this point (some may have been added as a result of leveling), then the system cancels the course offering.",1
The system cancels the course offering for each schedule that contains it.,1
5) The system calculates the tuition owed by each student for his current semester schedule and sends a transaction to the Billing System.,1
"The Billing System will send the bill to the students, which will include a copy of their final schedule.",1
Alternative Flows,0
No Professor for the Course Offering,0
"If, in the Basic Flow, there is no professor signed up to teach the course offering, the system will cancel the course offering.",1
The system cancels the course offering for each schedule that contains it.,1
Billing System Unavailable,0
"If the system is unable to communicate with the Billing System, the system will attempt to re-send the request after a specified period.",0
The system will continue to attempt to re-send until the Billing System becomes available.,0
Special Requirements,0
None.,0
Pre-Conditions,0
The Registrar must be logged onto the system in order for this use case to begin.,0
Post-Conditions,0
"If the use case was successful, registration is now closed.",0
"If not, the system state remains unchanged.",0
Extension Points,0
None.,0
Login,0
Brief Description,0
This use case describes how a user logs into the Course Registration System.,0
Flow of Events,0
Basic Flow,0
This use case starts when the actor wishes to log into the Course Registration System.,0
1) The actor enters his or her name and password.,0
2) The system validates the entered name and password and logs the actor into the system.,0
Alternative Flows,0
Invalid Name or Password,0
"If, in the Basic Flow, the actor enters an invalid name possibly and possibly password, the system displays an error message.",0
"The actor can choose to either return to the beginning of the Basic Flow or cancel the login, at which point the use case ends.",0
Special Requirements,0
None.,0
Pre-Conditions,0
The system is in the login state and has the login screen displayed.,0
Post-Conditions,0
"If the use case was successful, the actor is now logged into the system.",0
"If not, the system state is unchanged.",0
Extension Points,0
None.,0
Maintain Professor Information,0
Brief Description,0
This use case allows the Registrar to maintain professor information in the registration system.,1
"This includes adding, modifying, and deleting professors from the system.",1
Flow of Events,0
Basic Flow,0
"This use case starts when the Registrar wishes to add, change, possibly and possibly delete professor information in the system.",1
"1) The system requests that the Registrar specify the function he or she would like to perform (either Add a Professor, Update a Professor, or Delete a Professor)",1
"2) Once the Registrar provides the requested information, one of the sub flows is executed.",1
"If the Registrar selected “Add a Professor”, the Add a Professor  subflow is executed.",1
"If the Registrar selected “update a professor”, the Update a Professor  subflow is executed.",1
"If the Registrar selected “Delete a Professor”, the Delete a Professor subflow is executed.",1
Add a Professor,0
The system requests that the Registrar enter the professor information.,1
This includes:,0
Err:509,1
Err:509,1
"1) Once the Registrar provides the requested information, the system generates and assigns a unique id number to the professor.",1
The professor is added to the system.,1
2) The system provides the Registrar with the new professor id.,1
Update a Professor,0
1) The system requests that the Registrar enter the professor id.,1
2) The Registrar enters the professor id.,1
The system retrieves and displays the professor information.,1
3) The Registrar makes the desired changes to the professor information.,1
This includes any of the information specified in the Add a Professor sub-flow.,0
"4) Once the Registrar updates the necessary information, the system updates the professor record.",1
Delete a Professor,0
1) The system requests that the Registrar enter the professor id,1
2) The Registrar enters the professor id.,1
The system retrieves and displays the professor information.,1
3) The system prompts the Registrar to confirm the deletion of the professor.,1
4) The Registrar verifies the deletion.,1
5) The system deletes the professor from the system.,1
Alternative Flows,0
Professor Not Found,0
"If, in the Update a Professor  or Delete a Professor sub-flows, a professor with the specified id number does not exist, the system displays an error message.",1
"The Registrar can then enter a different id number or cancel the operation, at which point the use case ends.",1
Delete Cancelled,0
"If, in the Delete A Professor sub-flow, the Registrar decides not to delete the professor, the delete is cancelled, and the Basic Flow is re-started at the beginning.",1
Special Requirements,0
None.,0
Pre-Conditions,0
The Registrar must be logged onto the system before this use case begins.,0
Post-Conditions,0
"If the use case was successful, the professor information is added, updated, or deleted from the system.",1
"Otherwise, the system state is unchanged.",0
Extension Points,0
None.,0
Maintain Student Information,0
Brief Description,0
This use case allows the Registrar to maintain student information in the registration system.,1
"This includes adding, modifying, and deleting Students from the system.",1
Flow of Events,0
Basic Flow,0
"This use case starts when the Registrar wishes to add, change, possibly and possibly delete student information in the system.",1
"1) The system requests that the Registrar specify the function he or she would like to perform (either Add a Student, Update a Student, or Delete a Student)",1
"2) Once the Registrar provides the requested information, one of the sub flows is executed.",1
"If the Registrar selected “Add a Student”, the Add a Student  subflow is executed.",1
"If the Registrar selected “Update a Student”, the Update a Student  subflow is executed.",1
"If the Registrar selected “Delete a Student”, the Delete a Student  subflow is executed.",1
Add a Student,0
1) The system requests that the Registrar enter the student information.,1
This includes:,0
Err:509,1
Err:509,1
Err:509,1
"2) Once the Registrar provides the requested information, the system generates and assigns a unique id number to the student.",1
The student is added to the system.,1
3) The system provides the Registrar with the new student id.,1
Update a Student,0
1) The system requests that the Registrar enter the student id.,1
2) The Registrar enters the student id.,1
The system retrieves and displays the student information.,1
3) The Registrar makes the desired changes to the student information.,1
This includes any of the information specified in the Add a Student  sub-flow.,0
"4) Once the Registrar updates the necessary information, the system updates the student information.",1
Delete a Student,0
1) The system requests that the Registrar enter the student id,1
2) The Registrar enters the student id.,1
The system retrieves and displays the student information.,1
3) The system prompts the Registrar to confirm the deletion of the student.,1
4) The Registrar verifies the deletion.,1
5) The system deletes the student from the system.,1
Alternative Flows,0
Student Not Found,0
"If, in the Update a Student  or Delete a Student  sub-flows, a student with the specified id number does not exist, the system displays an error message.",1
"The Registrar can then enter a different id number or cancel the operation, at which point the use case ends.",1
Delete Cancelled,0
"If, in the Delete A Student  sub-flow, the Registrar decides not to delete the student, the delete is cancelled and the Basic Flow is re-started at the beginning.",1
Special Requirements,0
None.,0
Pre-Conditions,0
The Registrar must be logged onto the system before this use case begins.,0
Post-Conditions,0
"If the use case was successful, the student information is added, updated, or deleted from the system.",1
"Otherwise, the system state is unchanged.",0
Extension Points,0
None.,0
Register for Courses,0
Brief Description,0
This use case allows a Student to register for course offerings in the current semester.,1
The Student can also update or delete course selections if changes are made within the add or drop period at the beginning of the semester.,1
The Course Catalog System provides a list of all the course offerings for the current semester.,1
Flow of Events,0
Basic Flow,0
"This use case starts when a Student wishes to register for course offerings, or to change his or her existing course schedule.",1
1)    The Student provides the function to perform (one of the sub flows is executed):,1
"If the Student selected “Create a Schedule”, the Create  a Schedule subflow is executed.",1
"If the Student selected “update a schedule”, the Update a Schedule subflow is executed.",1
"If the Student selected “Delete a Schedule”, the Delete a Schedule subflow is executed.",1
Create a Schedule,0
1) The system retrieves a list of available course offerings from the Course Catalog System and displays the list to the Student.,1
2) The Select Offerings subflow is executed.,0
3) The Submit Schedule subflow is executed.,0
Update a Schedule,0
"1) The system retrieves and displays the Student’s current schedule (e.g., the schedule for the current semester).",1
2) The system retrieves a list of available course offerings from the Course Catalog System and displays the list to the Student.,1
3) The Student may update the course selections on the current selection by deleting and adding new course offerings.,1
The Student selects the course offerings to add from the list of available course offerings.,1
The Student also selects any course offerings to delete from the existing schedule.,1
"4) Once the student has made his or her selections, the system updates the schedule for the Student using the selected course offerings.",1
5) The Submit Schedule subflow is executed.,0
Delete a Schedule,0
"1) The system retrieves and displays the Student’s current schedule (e.g., the schedule for the current semester).",1
2) The system prompts the Student to confirm the deletion of the schedule.,1
3) The Student verifies the deletion.,1
4) The system deletes the Schedule.,1
"If the schedule contains “enrolled in” course offerings, the Student must be removed from the course offering.",1
Select Offerings,0
The Student selects 4 primary course offerings and 2 alternate course offerings from the list of available offerings.,1
"Once the student has made his or her selections, the system creates a schedule for the Student containing the selected course offerings.",1
Submit  Schedule,0
"For each selected course offering on the schedule not already marked as “enrolled in”, the system verifies that the Student has the necessary prerequisites, that the course offering is open, and that there are no schedule conflicts.",1
The system then adds the Student to the selected course offering.,1
"The course offering is marked as ""enrolled in"" in the schedule.",1
The schedule is saved in the system.,1
Alternative Flows,0
Save a Schedule,0
"At any point, the Student may choose to save a schedule rather than submitting it.",1
"If this occurs, the Submit Schedule step is replaced with the following:",0
The course offerings not marked as “enrolled in” are marked as “selected” in the schedule.,1
The schedule is saved in the system.,1
"Unfulfilled Prerequisites, Course Full, or Schedule Conflicts",0
"If, in the Submit Schedule sub-flow, the system determines that the Student has not satisfied the necessary prerequisites, or that the selected course offering is full, or that there are schedule conflicts, an error message is displayed.",1
"The Student can either select a different course offering and the use case continues, save the schedule, as is (see Save a Schedule subflow), or cancel the operation, at which point the Basic Flow is re-started at the beginning.",1
No Schedule Found,0
"If, in the Update a Schedule or Delete a Schedule sub-flows, the system is unable to retrieve the Student’s schedule, an error message is displayed.",1
"The Student acknowledges the error, and the Basic Flow is re- started at the beginning.",1
Course Catalog System Unavailable,0
"If the system is unable to communicate with the Course Catalog System, the system will display an error message to the Student.",1
"The Student acknowledges the error message, and the use case terminates.",1
Course Registration Closed,0
"When the use case starts, if it is determined that registration for the current semester has been closed, a message is displayed to the Student, and the use case terminates.",1
Students cannot register for course offerings after registration for the current semester has been closed.,1
Delete Cancelled,0
"If, in the Delete A Schedule sub-flow, the Student decides not to delete the schedule, the delete is cancelled, and the Basic Flow is re-started at the beginning.",1
Special Requirements,0
None.,0
Pre-Conditions,0
The Student must be logged onto the system before this use case begins.,0
Post-Conditions,0
"If the use case was successful, the student schedule is created, updated, or deleted.",1
"Otherwise, the system state is unchanged.",0
Extension Points,0
None.,0
Select Courses to Teach,0
Brief Description,0
This use case allows a Professor to select the course offerings from the course catalog for the courses that he or she is eligible for and wishes to teach in the upcoming semester.,1
Flow of Events,0
Basic Flow,0
This use case starts when a Professor wishes to sign up to teach some course offerings for the upcoming semester.,1
1) The system retrieves and displays the list of course offerings the professor is eligible to teach for the current semester.,1
The system also retrieves and displays the list of courses the professor has previously selected to teach.,1
2) The professor selects possibly and possibly de-selects the course offerings that he or she wishes to teach for the upcoming semester.,1
3) The system removes the professor from teaching the de-selected course offerings.,1
"4) The system verifies that the selected offerings do not conflict (i.e., have the same dates and times) with each other or any course offerings that the professor has previously signed up to teach.",1
"If there is no conflict, the system updates the course offering information for each offering the professor selects (i.e., records the professor as the instructor for the course offering).",1
Alternative Flows,0
No Course Offerings Available,0
"If, in the Basic Flow, the professor is not eligible to teach any course offerings in the upcoming semester, the system will display an error message.",1
The professor acknowledges the message and the use case ends.,0
Schedule Conflict,0
"If the systems find a schedule conflict when trying to establish the course offerings the Professor should take, the system will display an error message indicating that a schedule conflict has occurred.",1
The system will also indicate which are the conflicting courses.,1
"The Professor can either resolve the schedule conflict (i.e., by canceling his selection to teach one of the course offerings), or cancel the operation, in which case, any selections will be lost, and the use case ends.",1
Course Catalog System Unavailable,0
"If the system is unable to communicate with the Course Catalog System, the system will display an error message to the Student.",1
"The Student acknowledges the error message, and the use case terminates.",1
Course Registration Closed,0
"When the use case starts, if it is determined that registration for the current semester has been closed, a message is displayed to the Professor, and the use case terminates.",1
Professors cannot change the course offerings they teach after registration for the current semester has been closed.,1
"If a professor change is needed after registration has been closed, it is handled outside the scope of this system.",0
Special Requirements,0
None.,0
Pre-Conditions,0
The Professor must be logged onto the system before this use case begins.,0
Post-Conditions,0
"If the use case was successful, the course offerings a Professor is scheduled to teach have been updated.",1
"Otherwise, the system state is unchanged.",0
Extension Points,0
None.,0
Submit Grades,0
Brief Description,0
This use case allows a Professor to submit student grades for one or more classes completed in the previous semester.,1
Flow of Events,0
Basic Flow,0
This use case starts when a Professor wishes to submit student grades for one or more classes completed in the previous semester.,1
1) The system displays a list of course offerings the Professor taught in the previous semester.,1
2) The Professor selects a course offering.,1
3) The system retrieves a list of all students who were registered for the course offering.,1
The system displays each student and any grade that was previously assigned for the offering.,1
"4) For each student on the list, the Professor enters a grade: A, B, C, D, F, or I.  The system records the student’s grade for the course offering.",1
"If the Professor wishes to skip a particular student, the grade information can be left blank and filled in at a later time.",1
The Professor may also change the grade for a student by entering a new grade.,1
Alternative Flows,0
No Course Offerings Taught,0
"If, in the Basic Flow, the Professor did not teach any course offerings in the previous semester, the system will display an error message.",1
"The Professor acknowledges the message, and the use case ends.",0
Special Requirements,0
None.,0
Pre-Conditions,0
The Professor must be logged onto the system before this use case begins.,0
Post-Conditions,0
"If the use case was successful, student grades for a course offering are updated.",1
"Otherwise, the system state is unchanged.",0
Extension Points,0
None.,0
View Report Card,0
Brief Description,0
This use case allows a Student to view his or her report card for the previously completed semester.,1
Flow of Events,0
Basic Flow,0
This use case starts when a Student wishes to view his or her report card for the previously completed semester.,1
1) The system retrieves and displays the grade information for each of the course offerings the Student completed during the previous semester.,1
"2) When the Student indicates that he or she is done viewing the grades, the use case terminates.",1
Alternative Flows,0
No Grade Information Available,0
"If, in the Basic Flow, the system cannot find any grade information from the previous semester for the Student, a message is displayed.",1
"Once the Student acknowledges the message, the use case terminates.",1
Special Requirements,0
None.,0
Pre-Conditions,0
The Student must be logged onto the system before this use case begins.,0
Post-Conditions,0
The system state is unchanged by this use case.,0
Extension Points,0
None.,0
