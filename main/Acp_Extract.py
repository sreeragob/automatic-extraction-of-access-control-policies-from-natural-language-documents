import pandas as pd
import numpy as np
import spacy 
import json
from spacy import displacy 
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.layers import Dense
from tensorflow.keras import Sequential
from sklearn.feature_extraction.text import TfidfVectorizer		
from sklearn.metrics import classification_report, confusion_matrix
import tkinter as tk
from tkinter import filedialog
import tkinter.font as font

pd.set_option('display.max_colwidth', 500)



#for selecting csv file
def UploadAction(event=None):
    global filename   
    filename = filedialog.askopenfilename()
    print('Selected:', filename)

    
gui = tk.Tk(className=' CSV File Selector')
gui.geometry("500x200")
gui.configure(background='#FBB014')
myFont = font.Font(family='Helvetica', size=20, weight='bold')
button = tk.Button(gui, text='Open', command=UploadAction, fg='#FBB014', bg='black')
button['font'] = myFont
button.pack(pady=70)

gui.mainloop()

# load spaCy model #FBB014 Helvetica #DF1018 #509224
nlp = spacy.load("en_core_web_sm")


text = pd.read_csv(filename)
# create the transform
vectorizer = TfidfVectorizer()
# tokenize and build vocab
vectorizer.fit(text)
def ChunkIterator(Filename1):
	for chunk in pd.read_csv( filename, chunksize=1):
		for input1 in chunk['Sentence'].values:
			yield input1
		
corpus  = ChunkIterator(filename)
result=vectorizer.fit_transform(corpus)
X=result
y=text['AccessControl']
a=list(text['Sentence'])
print(len(a))
# ensure all data are floating point values	
X = X.astype('float32')
# encode strings to integer
y = LabelEncoder().fit_transform(y)
# split into train and test datasets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3,random_state=45)
print(X_train.shape, X_test.shape, y_train.shape, y_test.shape)
# determine the number of input features
n_features = X_train.shape[1]

model = Sequential()
model.add(Dense(10, activation='relu', kernel_initializer='he_normal', input_shape=(n_features,)))
model.add(Dense(8, activation='relu', kernel_initializer='he_normal'))
model.add(Dense(1, activation='sigmoid'))
y_train=np.asarray(y_train)
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
history=model.fit(X_train, y_train, epochs=150, batch_size=32, verbose=0)


# evaluate the model
loss, acc = model.evaluate(X_test, y_test, verbose=0)

# make a prediction
y_pred=model.predict(X_test)

labels=[0,1]

                                     
extrtd_acp_filename="acp.txt"

#writing the extracted access conrol policies to 'acp.txt'
fileacp = open(extrtd_acp_filename,"w")
j=len(a)-int(0.3*len(a))
l=0
for i in range(0,len(y_pred)):
	if (y_pred[i]>0.5 and (j+i)<len(a)):
		fileacp.write(a[j+i])
		fileacp.write("\n")
fileacp.close()


model.save('deep1.h5')
print('Test Accuracy: %.3f\n' % acc)
model.summary()
fileacp = open(extrtd_acp_filename,"r")
vislz=fileacp.read()
fileacp.close()

#To convert in to json format
dict1={}
with open(extrtd_acp_filename) as fh: 
  
    for line in fh: 
  
        # reads each line and trims of extra the spaces  
        # and gives only the valid words 
        command, description = line.strip().split(None, 1) 
  
        dict1[command] = description.strip() 
  
# creating json file 
# the JSON file is named as test1 
out_file = open("test1.json", "w") 
json.dump(dict1, out_file, indent = 4, sort_keys = False) 
out_file.close() 

#To visualise the Dependency parse 
doc=nlp(vislz)
sentence_spans = list(doc.sents)
displacy.serve(sentence_spans, style="dep")




